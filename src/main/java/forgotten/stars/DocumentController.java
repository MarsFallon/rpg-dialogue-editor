package forgotten.stars;

import com.fasterxml.jackson.databind.ObjectMapper;
import forgotten.stars.dialogue.writer.MapEditor;
import forgotten.stars.display.EditorGUI;
import forgotten.stars.display.listener.*;
import forgotten.stars.rpg.Event;
import forgotten.stars.rpg.EventPage.EventPage;
import forgotten.stars.rpg.Map;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class DocumentController {
    private Map map;
    private ObjectMapper mapper;
    private int eventId = 1;
    private int pageId = 0;

    private File mapFile;

    private EditorGUI gui;

    public DocumentController() {
        mapper = new ObjectMapper();
        gui = new EditorGUI();
    }

    public void setMapFile(File readFile) {
        mapFile = readFile;
        try {
            map = mapper.readValue(readFile, Map.class);
            eventId = 1;
            pageId = 0;
            populateGui();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void save() {
        try {
            mapper.writeValue(mapFile, map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateText(String text) {
        MapEditor.writeTextToMap(map, eventId, pageId, text);
    }

    public void start() {
        gui.start();
        attachActionListeners();
    }

    private void populateGui() {
        List<Event> events = map.getEvents();
        gui.populateEventDropDown(events);
        List<EventPage> pages = events.get(eventId).getPages();
        gui.populatePageDropDown(pages.size());
        gui.setText(MapEditor.getTextFromMap(map, eventId, pageId));
    }

    private void attachActionListeners() {
        OpenListener openListener = new OpenListener();
        openListener.setController(this);
        gui.setOpenButtonListener(openListener);

        SaveListener saveListener = new SaveListener();
        saveListener.setController(this);
        gui.setSaveButtonListener(saveListener);

        WriteListener writeListener = new WriteListener();
        writeListener.setController(this);
        gui.setDocumentListener(writeListener);

        EventSelectListener eventDropdownListener = new EventSelectListener();
        eventDropdownListener.setController(this);
        gui.setEventDropdownListener(eventDropdownListener);

        PageSelectListener pageSelectListener = new PageSelectListener();
        pageSelectListener.setController(this);
        gui.setPageDropdownListener(pageSelectListener);
    }

    public void selectEvent(int eventNumber) {
        if (eventNumber < 0) {
            eventNumber = 0;
        }
        if (eventId != eventNumber) {
            eventId = eventNumber;
            pageId = 0;
            gui.setText(MapEditor.getTextFromMap(map, eventId, pageId));
            List<Event> events = map.getEvents();
            List<EventPage> pages = events.get(eventId).getPages();
            gui.populatePageDropDown(pages.size());
            gui.refresh();
        }
    }

    public void selectPage(int pageNumber) {
        if (pageNumber < 0) {
            pageNumber = 0;
        }
        if (pageId != pageNumber) {
            pageId = pageNumber;
            gui.setText(MapEditor.getTextFromMap(map, eventId, pageId));
            gui.refresh();
        }
    }

    public void save(File selectedFile) {
        try {
            mapper.writeValue(selectedFile, map);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

