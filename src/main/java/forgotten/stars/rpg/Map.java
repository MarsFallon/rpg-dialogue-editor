package forgotten.stars.rpg;

import java.util.List;

public class Map {
    private boolean autoplayBgm;
    private boolean autoplayBgs;
    private String battleback1Name;
    private String battleback2Name;
    private AudioFile bgm;
    private AudioFile bgs;
    private List<Integer> data;
    private boolean disableDashing;
    private String displayName;
    private List<Encounter> encounterList;
    private int encounterStep;
    private List<Event> events;
    private int height;
    private String note;
    private boolean parallaxLoopX;
    private boolean parallaxLoopY;
    private String parallaxName;
    private boolean parallaxShow;
    private int parallaxSx;
    private int parallaxSy;
    private int scrollType;
    private boolean specifyBattleback;
    private int tilesetId;
    private int width;

    public boolean isAutoplayBgm() {
        return autoplayBgm;
    }

    public void setAutoplayBgm(boolean autoplayBgm) {
        this.autoplayBgm = autoplayBgm;
    }

    public boolean isAutoplayBgs() {
        return autoplayBgs;
    }

    public void setAutoplayBgs(boolean autoplayBgs) {
        this.autoplayBgs = autoplayBgs;
    }

    public String getBattleback1Name() {
        return battleback1Name;
    }

    public void setBattleback1Name(String battleback1Name) {
        this.battleback1Name = battleback1Name;
    }

    public String getBattleback2Name() {
        return battleback2Name;
    }

    public void setBattleback2Name(String battleback2Name) {
        this.battleback2Name = battleback2Name;
    }

    public AudioFile getBgm() {
        return bgm;
    }

    public void setBgm(AudioFile bgm) {
        this.bgm = bgm;
    }

    public AudioFile getBgs() {
        return bgs;
    }

    public void setBgs(AudioFile bgs) {
        this.bgs = bgs;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }

    public boolean isDisableDashing() {
        return disableDashing;
    }

    public void setDisableDashing(boolean disableDashing) {
        this.disableDashing = disableDashing;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public List<Encounter> getEncounterList() {
        return encounterList;
    }

    public void setEncounterList(List<Encounter> encounterList) {
        this.encounterList = encounterList;
    }

    public int getEncounterStep() {
        return encounterStep;
    }

    public void setEncounterStep(int encounterStep) {
        this.encounterStep = encounterStep;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isParallaxLoopX() {
        return parallaxLoopX;
    }

    public void setParallaxLoopX(boolean parallaxLoopX) {
        this.parallaxLoopX = parallaxLoopX;
    }

    public boolean isParallaxLoopY() {
        return parallaxLoopY;
    }

    public void setParallaxLoopY(boolean parallaxLoopY) {
        this.parallaxLoopY = parallaxLoopY;
    }

    public String getParallaxName() {
        return parallaxName;
    }

    public void setParallaxName(String parallaxName) {
        this.parallaxName = parallaxName;
    }

    public boolean isParallaxShow() {
        return parallaxShow;
    }

    public void setParallaxShow(boolean parallaxShow) {
        this.parallaxShow = parallaxShow;
    }

    public int getParallaxSx() {
        return parallaxSx;
    }

    public void setParallaxSx(int parallaxSx) {
        this.parallaxSx = parallaxSx;
    }

    public int getParallaxSy() {
        return parallaxSy;
    }

    public void setParallaxSy(int parallaxSy) {
        this.parallaxSy = parallaxSy;
    }

    public int getScrollType() {
        return scrollType;
    }

    public void setScrollType(int scrollType) {
        this.scrollType = scrollType;
    }

    public boolean isSpecifyBattleback() {
        return specifyBattleback;
    }

    public void setSpecifyBattleback(boolean specifyBattleback) {
        this.specifyBattleback = specifyBattleback;
    }

    public int getTilesetId() {
        return tilesetId;
    }

    public void setTilesetId(int tilesetId) {
        this.tilesetId = tilesetId;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
}
