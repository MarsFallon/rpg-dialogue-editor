package forgotten.stars.rpg;

import forgotten.stars.rpg.EventPage.EventPage;

import java.util.List;

public class Event {
    private int id;
    private String name;
    private String note;
    private List<EventPage> pages;
    private int x;
    private int y;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<EventPage> getPages() {
        return pages;
    }

    public void setPages(List<EventPage> pages) {
        this.pages = pages;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
