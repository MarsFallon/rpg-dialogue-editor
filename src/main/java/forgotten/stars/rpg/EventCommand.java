package forgotten.stars.rpg;

import java.util.ArrayList;
import java.util.List;

public class EventCommand {
    int code;
    int indent;
    List<Object> parameters;

    public void createParametersFromString(String parameter) {
        parameters = new ArrayList<>();
        parameters.add(parameter);
    }

    public int getCode() {
        return code;
    }

    public int getIndent() {
        return indent;
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public void setIndent(int indent) {
        this.indent = indent;
    }

    public void setParameters(List<Object> parameters) {
        this.parameters = parameters;
    }

    public void insertParameter(Object parameter) {
        parameters.add(parameter);
    }
}
