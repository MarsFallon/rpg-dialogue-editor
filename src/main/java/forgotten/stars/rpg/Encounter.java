package forgotten.stars.rpg;

import java.util.List;

public class Encounter {
    private List<Integer> regionSet;
    private int troopId;
    private int weight;

    public List<Integer> getRegionSet() {
        return regionSet;
    }

    public void setRegionSet(List<Integer> regionSet) {
        this.regionSet = regionSet;
    }

    public int getTroopId() {
        return troopId;
    }

    public void setTroopId(int troopId) {
        this.troopId = troopId;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
