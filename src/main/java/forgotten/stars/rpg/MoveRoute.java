package forgotten.stars.rpg;

import java.util.List;

public class MoveRoute {
    private List<MoveCommand> list;
    private boolean repeat;
    private boolean skippable;
    private boolean wait;

    public List<MoveCommand> getList() {
        return list;
    }

    public void setList(List<MoveCommand> list) {
        this.list = list;
    }

    public boolean isRepeat() {
        return repeat;
    }

    public void setRepeat(boolean repeat) {
        this.repeat = repeat;
    }

    public boolean isSkippable() {
        return skippable;
    }

    public void setSkippable(boolean skippable) {
        this.skippable = skippable;
    }

    public boolean isWait() {
        return wait;
    }

    public void setWait(boolean wait) {
        this.wait = wait;
    }
}
