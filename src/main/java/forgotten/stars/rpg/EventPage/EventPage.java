package forgotten.stars.rpg.EventPage;

import forgotten.stars.rpg.EventCommand;
import forgotten.stars.rpg.MoveRoute;

import java.util.List;

public class EventPage {
    Conditions conditions;
    boolean directionFix;
    Image image;
    List<EventCommand> list;
    int moveFrequency;
    MoveRoute moveRoute;
    int moveSpeed;
    int moveType;
    int priorityType;
    boolean stepAnime;
    boolean through;
    int trigger;
    boolean walkAnime;

    public Conditions getConditions() {
        return conditions;
    }

    public void setConditions(Conditions conditions) {
        this.conditions = conditions;
    }

    public boolean isDirectionFix() {
        return directionFix;
    }

    public void setDirectionFix(boolean directionFix) {
        this.directionFix = directionFix;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<EventCommand> getList() {
        return list;
    }

    public void setList(List<EventCommand> list) {
        this.list = list;
    }

    public int getMoveFrequency() {
        return moveFrequency;
    }

    public void setMoveFrequency(int moveFrequency) {
        this.moveFrequency = moveFrequency;
    }

    public MoveRoute getMoveRoute() {
        return moveRoute;
    }

    public void setMoveRoute(MoveRoute moveRoute) {
        this.moveRoute = moveRoute;
    }

    public int getMoveSpeed() {
        return moveSpeed;
    }

    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    public int getMoveType() {
        return moveType;
    }

    public void setMoveType(int moveType) {
        this.moveType = moveType;
    }

    public int getPriorityType() {
        return priorityType;
    }

    public void setPriorityType(int priorityType) {
        this.priorityType = priorityType;
    }

    public boolean isStepAnime() {
        return stepAnime;
    }

    public void setStepAnime(boolean stepAnime) {
        this.stepAnime = stepAnime;
    }

    public boolean isThrough() {
        return through;
    }

    public void setThrough(boolean through) {
        this.through = through;
    }

    public int getTrigger() {
        return trigger;
    }

    public void setTrigger(int trigger) {
        this.trigger = trigger;
    }

    public boolean isWalkAnime() {
        return walkAnime;
    }

    public void setWalkAnime(boolean walkAnime) {
        this.walkAnime = walkAnime;
    }
}
