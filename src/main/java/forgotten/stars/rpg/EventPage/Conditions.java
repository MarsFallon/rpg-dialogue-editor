package forgotten.stars.rpg.EventPage;

public class Conditions {
    private int actorId;
    private boolean actorValid;
    private int itemId;
    private boolean itemValid;
    private String selfSwitchCh;
    private boolean selfSwitchValid;
    private int switch1Id;
    private boolean switch1Valid;
    private int switch2Id;
    private boolean switch2Valid;
    private int variableId;
    private boolean variableValid;
    private int variableValue;

    public int getActorId() {
        return actorId;
    }

    public void setActorId(int actorId) {
        this.actorId = actorId;
    }

    public boolean isActorValid() {
        return actorValid;
    }

    public void setActorValid(boolean actorValid) {
        this.actorValid = actorValid;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public boolean isItemValid() {
        return itemValid;
    }

    public void setItemValid(boolean itemValid) {
        this.itemValid = itemValid;
    }

    public String getSelfSwitchCh() {
        return selfSwitchCh;
    }

    public void setSelfSwitchCh(String selfSwitchCh) {
        this.selfSwitchCh = selfSwitchCh;
    }

    public boolean isSelfSwitchValid() {
        return selfSwitchValid;
    }

    public void setSelfSwitchValid(boolean selfSwitchValid) {
        this.selfSwitchValid = selfSwitchValid;
    }

    public int getSwitch1Id() {
        return switch1Id;
    }

    public void setSwitch1Id(int switch1Id) {
        this.switch1Id = switch1Id;
    }

    public boolean isSwitch1Valid() {
        return switch1Valid;
    }

    public void setSwitch1Valid(boolean switch1Valid) {
        this.switch1Valid = switch1Valid;
    }

    public int getSwitch2Id() {
        return switch2Id;
    }

    public void setSwitch2Id(int switch2Id) {
        this.switch2Id = switch2Id;
    }

    public boolean isSwitch2Valid() {
        return switch2Valid;
    }

    public void setSwitch2Valid(boolean switch2Valid) {
        this.switch2Valid = switch2Valid;
    }

    public int getVariableId() {
        return variableId;
    }

    public void setVariableId(int variableId) {
        this.variableId = variableId;
    }

    public boolean isVariableValid() {
        return variableValid;
    }

    public void setVariableValid(boolean variableValid) {
        this.variableValid = variableValid;
    }

    public int getVariableValue() {
        return variableValue;
    }

    public void setVariableValue(int variableValue) {
        this.variableValue = variableValue;
    }
}
