package forgotten.stars.rpg;

import java.util.List;

public class MoveCommand {
    private int code;
    private List<Object> parameters;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<Object> getParameters() {
        return parameters;
    }

    public void setParameters(List<Object> parameters) {
        this.parameters = parameters;
    }
}
