package forgotten.stars.display.listener;

import forgotten.stars.DocumentController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EventSelectListener implements ActionListener {
    private DocumentController controller;

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox<Integer> dropdown = (JComboBox<Integer>) e.getSource();
        Object selected = dropdown.getSelectedItem();
        if (selected != null) {
            int eventNumber = (Integer)selected;
            controller.selectEvent(eventNumber);
        }
    }

    public void setController(DocumentController controller) {
        this.controller = controller;
    }
}
