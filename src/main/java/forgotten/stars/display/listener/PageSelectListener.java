package forgotten.stars.display.listener;

import forgotten.stars.DocumentController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PageSelectListener implements ActionListener {
    private DocumentController controller;

    @Override
    public void actionPerformed(ActionEvent e) {
        JComboBox<Integer> dropdown = (JComboBox<Integer>) e.getSource();
        Object selected = dropdown.getSelectedIndex();
        if (selected != null) {
            int pageNumber = (Integer)selected;
            controller.selectPage(pageNumber);
        }
    }

    public void setController(DocumentController controller) {
        this.controller = controller;
    }
}
