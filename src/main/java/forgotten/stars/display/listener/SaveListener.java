package forgotten.stars.display.listener;

import forgotten.stars.DocumentController;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class SaveListener implements ActionListener {
    private DocumentController controller;

    @Override
    public void actionPerformed(ActionEvent e) {
        JFileChooser jfc = new JFileChooser();
        int returnValue = jfc.showOpenDialog(null);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            File selectedFile = jfc.getSelectedFile();
            controller.save(selectedFile);
        }
    }

    public void setController(DocumentController controller) {
        this.controller = controller;
    }
}
