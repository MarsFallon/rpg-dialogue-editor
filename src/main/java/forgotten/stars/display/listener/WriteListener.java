package forgotten.stars.display.listener;

import forgotten.stars.DocumentController;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public class WriteListener implements DocumentListener {
    private DocumentController controller;

    @Override
    public void insertUpdate(DocumentEvent e) {
        updateMap(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        updateMap(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {

    }

    private void updateMap(DocumentEvent e) {
        Document d = e.getDocument();
        try {
            controller.updateText(d.getText(0, d.getLength()));
            controller.save();
        } catch (BadLocationException e1) {
            e1.printStackTrace();
        }

    }

    public void setController(DocumentController controller) {
        this.controller = controller;
    }
}
