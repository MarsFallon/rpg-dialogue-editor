package forgotten.stars.display;

import forgotten.stars.rpg.Event;
import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import java.awt.event.ActionListener;
import java.util.List;

public class EditorGUI {
    private JTextArea textArea;
    private JFrame frame;
    private JButton openButton;
    private JButton saveButton;
    private JComboBox<Integer> eventDropDown;
    private JComboBox<Integer> pageDropDown;

    public EditorGUI() {
        frame = new JFrame();
        textArea = new JTextArea();
        initTextArea();
        eventDropDown = new JComboBox<>();
        pageDropDown = new JComboBox<>();

        saveButton = new JButton("save");
        saveButton.setBounds(130, 100, 100, 40);

        openButton = new JButton("open");
        openButton.setBounds(130, 100, 100, 40);

        frame.setSize(800,800);
        frame.setLayout(new MigLayout());
        frame.add(openButton);
        frame.add(new JLabel("Event:"));
        frame.add(eventDropDown);
        frame.add(new JLabel("Page:"));
        frame.add(pageDropDown, "wrap");
        frame.add(textArea, "span,wrap");
        frame.add(saveButton);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    private void initTextArea() {
        textArea.setEditable(true);
        textArea.setLineWrap(true);
        textArea.setWrapStyleWord(true);
        textArea.setSize(750, 750);

    }

    public void start(){
        frame.setVisible(true);
    }

    public void setText(String text) {
        textArea.setText(text);
        frame.revalidate();
        frame.repaint();
    }

    public void populateEventDropDown(List<Event> events) {
        eventDropDown.removeAllItems();

        for (Event event : events) {
            if (event != null) {
                eventDropDown.addItem(event.getId());
            }
        }
    }

    public void populatePageDropDown(int numPages) {
        pageDropDown.removeAllItems();

        for (int i = 1; i <= numPages; i++) {
            pageDropDown.addItem(i);
        }
    }

    public void setSaveButtonListener(ActionListener listener) {
        saveButton.addActionListener(listener);
    }

    public void setOpenButtonListener(ActionListener listener) {
        openButton.addActionListener(listener);
    }

    public void setDocumentListener(DocumentListener listener) {
        Document d = textArea.getDocument();
        d.addDocumentListener(listener);
    }

    public void setEventDropdownListener(ActionListener listener) {
        eventDropDown.addActionListener(listener);
    }

    public void setPageDropdownListener(ActionListener listener) {
        pageDropDown.addActionListener(listener);
    }

    public void refresh() {
        frame.revalidate();
        frame.repaint();
    }
}
