package forgotten.stars.dialogue.writer;

import forgotten.stars.rpg.EventCommand;
import forgotten.stars.rpg.Map;

import java.util.ArrayList;
import java.util.List;

public class MapEditor {
    private static final int TEXT_BODY_CODE = 401;
    private static final int TEXT_WINDOW_CODE = 101;

    public static String getTextFromMap(Map map, int eventNumber, int eventPageNumber) {
        List<EventCommand> commands = map.getEvents().get(eventNumber).getPages().get(eventPageNumber).getList();
        String text = "";
        int lastcode = 0;
        for (EventCommand command : commands) {
            if (lastcode == TEXT_BODY_CODE) {
                if (command.getCode() != TEXT_BODY_CODE) {
                    break;
                }
                text = text + "\n";
            }
            if (command.getCode() == TEXT_BODY_CODE) {
                String s = (String)command.getParameters().get(0);
                text = text + s;
            }
            lastcode = command.getCode();
        }
        return text;
    }

    public static void writeTextToMap(Map map, int eventNumber, int eventPageNumber, String text) {
        List<EventCommand> commands = Parser.convertTextToCommands(text);

        map.getEvents().get(eventNumber).getPages().get(eventPageNumber).setList(commands);
    }
}
