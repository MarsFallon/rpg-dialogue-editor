package forgotten.stars.dialogue.writer;

import forgotten.stars.rpg.EventCommand;
import org.apache.commons.lang3.text.WordUtils;

import java.util.ArrayList;
import java.util.List;

public class Parser {
    private static final int LINE_LENGTH = 55;
    private static final int BOX_LENGTH = 4;

    private static final int TEXT_BODY_CODE = 401;
    private static final int TEXT_WINDOW_CODE = 101;
    private static final int TEXT_END_OF_WINDOW_CODE = 0;

    public static List<EventCommand> convertTextToCommands(String text) {
        text = WordUtils.wrap(text, LINE_LENGTH);

        String lines[] = text.split("\\r?\\n");

        List<EventCommand> commands = new ArrayList<>();

        int lineCounter = 0;
        for (String line : lines) {
            if (lineCounter == 0) {
                commands.add(getStartOfWindowCommand());
            }
            commands.add(getTextCommandFromString(line));
            lineCounter++;
            if (lineCounter >= BOX_LENGTH) {
                commands.add(getEndOfWindowCommand());
                lineCounter = 0;
            }
        }

        if (commands.get(commands.size()-1).getCode() != TEXT_END_OF_WINDOW_CODE) {
            commands.add(getEndOfWindowCommand());
        }

        return commands;
    }

    private static EventCommand getTextCommandFromString(String text) {
        EventCommand command = new EventCommand();
        command.setCode(TEXT_BODY_CODE);
        command.setIndent(0);
        command.createParametersFromString(text);
        return command;
    }

    private static EventCommand getStartOfWindowCommand() {
        EventCommand command = new EventCommand();
        command.setCode(TEXT_WINDOW_CODE);
        command.setIndent(0);
        List<Object> parameters = new ArrayList<>();
        parameters.add("");
        parameters.add(0);
        parameters.add(0);
        parameters.add(0);
        command.setParameters(parameters);
        return command;
    }

    private static EventCommand getEndOfWindowCommand() {
        EventCommand command = new EventCommand();
        command.setCode(TEXT_END_OF_WINDOW_CODE);
        command.setIndent(0);
        List<Object> parameters = new ArrayList<>();
        command.setParameters(parameters);
        return command;
    }
}
