package forgotten.stars.dialogue.writer;

import forgotten.stars.DocumentController;

public class Main {

    public static void main(String[] args) {
        DocumentController controller = new DocumentController();

        controller.start();
    }
}
