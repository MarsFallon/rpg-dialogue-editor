package forgotten.stars.dialogue.writer;

import forgotten.stars.rpg.EventCommand;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ParserTest {
    @Test
    public void parsesRawTextToEventCommands() {
        EventCommand command = new EventCommand();
        command.setCode(401);
        command.setIndent(0);
        List<Object> parameters = new ArrayList<>();
        parameters.add("Test");
        command.setParameters(parameters);

        List<EventCommand> output = Parser.convertTextToCommands("Test");
        assertEquals(401, output.get(1).getCode());
        assertEquals(0, output.get(1).getIndent());
        assertEquals("Test", output.get(1).getParameters().get(0));
    }

    @Test
    public void convertTextToCommands_AppliesWindowStartAndEnd() {
        List<EventCommand> output = Parser.convertTextToCommands("Test");

        assertEquals(101, output.get(0).getCode());
        assertEquals(0, output.get(output.size()-1).getCode());
    }

    @Test
    public void convertTextToCommands_WrapsLongLine() {
        List<EventCommand> output = Parser.convertTextToCommands(
                "Testing with this very long string that ought to wrap " +
                        "around to a new line because it's so very very long! " +
                        "My oh my, what a long string!");

        assertEquals(5, output.size());
    }

    @Test
    public void convertTextToCommands_WrapsMultipleBoxText() {
        List<EventCommand> output = Parser.convertTextToCommands(
                "Testing with this very long string that ought to wrap " +
                        "around to a new line because it's so very very long! My " +
                        "oh my, what a long string! Ah, and this is the special " +
                        "edition, even longer string, which must surpass four " +
                        " lines in order to test the rigorous wrapping.");

        assertEquals(9, output.size());
        assertEquals("lines in order to test the rigorous wrapping.", output.get(7).getParameters().get(0));
    }
}