package forgotten.stars.dialogue.writer;

import forgotten.stars.rpg.Event;
import forgotten.stars.rpg.EventCommand;
import forgotten.stars.rpg.EventPage.EventPage;
import forgotten.stars.rpg.Map;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MapEditorTest {

    private static final int TEXT_BODY_CODE = 401;
    private static final int TEXT_WINDOW_CODE = 101;

    @Test
    public void readOneLineTextBox() {
        List<EventCommand> commands = new ArrayList<>();
        commands.add(getCommand(TEXT_WINDOW_CODE, ""));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 1"));

        Map map = getOneEventMap(commands);

        String actual = MapEditor.getTextFromMap(map, 0, 0);
        String expected = "Line 1";

        assertEquals(actual, expected);
    }

    @Test
    public void readFourLineTextBox() {
        List<EventCommand> commands = new ArrayList<>();
        commands.add(getCommand(TEXT_WINDOW_CODE, ""));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 1"));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 2"));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 3"));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 4"));

        Map map = getOneEventMap(commands);

        String actual = MapEditor.getTextFromMap(map, 0, 0);
        String expected = "Line 1\n" +
                "Line 2\n" +
                "Line 3\n" +
                "Line 4";

        assertEquals(actual, expected);
    }

    @Test
    public void readFirstTextBoxOnly() {
        List<EventCommand> commands = new ArrayList<>();
        commands.add(getCommand(TEXT_WINDOW_CODE, ""));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 1"));
        commands.add(getCommand(TEXT_BODY_CODE, "Line 2"));
        commands.add(getCommand(TEXT_WINDOW_CODE, ""));
        commands.add(getCommand(TEXT_BODY_CODE, "Line A"));
        commands.add(getCommand(TEXT_BODY_CODE, "Line B"));

        Map map = getOneEventMap(commands);

        String actual = MapEditor.getTextFromMap(map, 0, 0);
        String expected = "Line 1\n" +
                "Line 2";

        assertEquals(actual, expected);
    }

    @Test
    public void writeText() {
        String s = "Test Text";
        List<EventCommand> commands = new ArrayList<>();

        Map map = getOneEventMap(commands);

        MapEditor.writeTextToMap(map, 0, 0, s);
        String actual = MapEditor.getTextFromMap(map, 0, 0);

        assertEquals(actual, s);
    }

    private Map getOneEventMap(List<EventCommand> commands) {
        Map map = new Map();
        EventPage page = new EventPage();
        page.setList(commands);
        List<EventPage> pages = new ArrayList<>();
        pages.add(page);
        Event event = new Event();
        event.setPages(pages);
        List<Event> events = new ArrayList<>();
        events.add(event);
        map.setEvents(events);
        return map;
    }

    private EventCommand getCommand(int code, String parameter) {
        EventCommand command = new EventCommand();
        command.setCode(code);
        command.createParametersFromString(parameter);
        return command;
    }
}